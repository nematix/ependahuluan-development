# EPendahuluan Development Node #

EPendahuluan Development Node repository for Docker deployment.

## Running an EPendahuluan Development Node

We have to make sure Docker & Docker-Compose is install on the development node. If everything is in place, then
we can proceed by cloning the EPendahuluan Development Node repository using following command.

    git clone https://nematix@bitbucket.org/marafoundry/ependahuluan-development.git ependahuluan
    cd ependahuluan/
    git submodule init
    git submodule update

 Start the containers.
    
    docker-compose -p ep up

We also can start containers in background by using following command

    docker-compose -p ep up -d

We can check the container is running using the following command

    docker-compose -p ep ps

Check logs of the container using below command

    docker-compose -p ep logs